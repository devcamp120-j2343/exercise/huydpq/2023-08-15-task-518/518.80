import styled from "styled-components";

const Button1 = styled.button`
  width: 25%;  
  background: #425062;  
  color: ${props => props.bgColor || "#fff"};  
  padding: 20px;    
  font-size: 20px;  
  margin-right: 0px;  
  border: solid 2px #3C4857; 
  
`

export { Button1 }