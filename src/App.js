import './App.css';
import "bootstrap/dist/css/bootstrap.min.css"
import { Button1 } from './components/button';
function App() {
  return (
    <div style={{ margin: '50px' }} >
      <div className='box'>
        <div className='display'>
          <input type="text" readonly size="18" />
        </div>

        <div className='keys'>
          <div className='button-row'>
            <Button1 bgColor={"red"}>C</Button1>
            <Button1 >?</Button1>
            <Button1>%</Button1>
            <Button1>/</Button1>
          </div>
          <div className='button-row'>
            <Button1>7</Button1>
            <Button1 >8</Button1>
            <Button1>9</Button1>
            <Button1>X</Button1>
          </div>
          <div className='button-row'>
            <Button1 >4</Button1>
            <Button1 >5</Button1>
            <Button1>6</Button1>
            <Button1>-</Button1>
          </div>
          <div className='button-row'>
            <Button1>1</Button1>
            <Button1 >2</Button1>
            <Button1>3</Button1>
            <Button1>+</Button1>
          </div>
          <div className='button-row'>
            <Button1>.</Button1>
            <Button1 >0</Button1>
            <Button1> ,</Button1>
            <Button1>=</Button1>
          </div>

        </div>
      </div>

    </div>
  );
}

export default App;
